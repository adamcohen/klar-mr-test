package logger

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	colorDebug = "\x1b[0;34m%s\x1b[0m"
	colorInfo  = "\x1b[0;32m%s\x1b[0m"
	colorWarn  = "\x1b[0;33m%s\x1b[0m"
	colorError = "\x1b[0;31m%s\x1b[0m"
	colorFatal = "\x1b[0;31m%s\x1b[0m"
)

// LogFormatter is used to by the logrus package to provide a custom logger
type LogFormatter struct {
}

// Format creates a custom log formatter so we can colorize and format the output
func (f *LogFormatter) Format(entry *log.Entry) ([]byte, error) {
	formattedLevel := strings.ToUpper(entry.Level.String()[0:4])

	colorFormatString := func() string {
		switch entry.Level {
		case log.InfoLevel:
			return colorInfo
		case log.WarnLevel:
			return colorWarn
		case log.ErrorLevel:
			return colorError
		case log.DebugLevel:
			return colorDebug
		case log.FatalLevel:
			return colorFatal
		}

		return colorError
	}()

	logEntry := fmt.Sprintf("[%s] ▶ %s\n", formattedLevel, entry.Message)
	coloredLogEntry := fmt.Sprintf(colorFormatString, logEntry)

	return []byte(coloredLogEntry), nil
}
