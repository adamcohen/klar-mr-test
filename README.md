# klar analyzer

GitLab Analyzer for Docker Containers.

This analyzer is a wrapper around [clair](https://github.com/coreos/clair/), a vulnerability static analysis for docker containers, utilizing [klar](https://github.com/optiopay/klar) to analyze images stored in a private or public Docker registry for security vulnerabilities.

## How to use

### Running the scanning tool

1. Run [docker desktop](https://www.docker.com/products/docker-desktop) or [docker-machine](https://github.com/docker/machine)
1. Run the latest [prefilled vulnerabilities database](https://cloud.docker.com/repository/docker/arminc/clair-db)
    ```sh
    docker run -p 5432:5432 -d --name clair-db arminc/clair-db:latest
    ```
1. Configure an environment variable to point to the ip address of your local machine (or just insert the appropriate ip address for the `CLAIR_VULNERABILITIES_DB_URL` in step `4.`)
   ```
   export LOCAL_MACHINE_IP_ADDRESS=`docker-machine ip default || ipconfig getifaddr en0`
   ```
1. Run the analyzer, passing the image and SHA you want to analyze in the `CI_APPLICATION_REPOSITORY` and `CI_APPLICATION_TAG` environment variables
    ```sh
    docker run \
      --interactive --rm \
      --volume "$PWD":/tmp/app \
      -e CI_PROJECT_DIR=/tmp/app -e CLAIR_VULNERABILITIES_DB_URL=$LOCAL_MACHINE_IP_ADDRESS \
      -e CI_APPLICATION_REPOSITORY=registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256 \
      -e CI_APPLICATION_TAG=bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e \
      registry.gitlab.com/gitlab-org/security-products/analyzers/klar
    ```

The results will be stored in `gl-container-scanning-report.json`.

### Using a whitelist

To use a whitelist, create a file named `clair-whitelist.yml` in the root directory of this project, with contents matching the [following whitelist format](https://github.com/arminc/clair-scanner/blob/v12/example-whitelist.yaml)

### Authentication in Docker registry

If the Docker registry where you pull an image from needs authentication, you can pass the credentials using `DOCKER_USER` and `DOCKER_PASSWORD` environment variables to the Container Scanning tool container.

### Debugging

You can pass the `KLAR_TRACE=true` environment variable to the `container-scanning` Docker container to see the shell wrapper trace output.

## Environment Variables

| Environment Variable           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Default                                  |
| ------                         | ------                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | ------                                   |
| `KLAR_TRACE`                   | Set to true to enable more verbose output from klar.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | `"false"`                                |
| `DOCKER_USER`                  | Username for accessing a Docker registry requiring authentication.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `$CI_REGISTRY_USER`                      |
| `DOCKER_PASSWORD`              | Password for accessing a Docker registry requiring authentication.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `$CI_REGISTRY_PASSWORD`                  |
| `CLAIR_OUTPUT`                 | Severity level threshold. Vulnerabilities with severity level higher than or equal to this threshold will be outputted. Supported levels are `Unknown`, `Negligible`, `Low`, `Medium`, `High`, `Critical` and `Defcon1`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | `Unknown`                                |
| `REGISTRY_INSECURE`            | Allow [Klar](https://github.com/optiopay/klar) to access insecure registries (HTTP only). Should only be set to `true` when testing the image locally.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `"false"`                                |
| `CLAIR_VULNERABILITIES_DB_URL` | This variable is explicitly set in the [services section](https://gitlab.com/gitlab-org/gitlab/blob/30522ca8b901223ac8c32b633d8d67f340b159c1/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml#L17-19) of the `Container-Scanning.gitlab-ci.yml` file and defaults to `clair-vulnerabilities-db`.  This value represents the address that the [postgres server hosting the vulnerabilities definitions](https://hub.docker.com/r/arminc/clair-db) is running on and **shouldn't be changed** unless you're running the image locally as described in the [Running the scanning tool](https://gitlab.com/gitlab-org/security-products/analyzers/klar/#running-the-scanning-tool) section of the [GitLab klar analyzer readme](https://gitlab.com/gitlab-org/security-products/analyzers/klar). | `clair-vulnerabilities-db`               |
| `CI_APPLICATION_REPOSITORY`    | Docker repository URL for the image to be scanned.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG` |
| `CI_APPLICATION_TAG`           | Docker respository tag for the image to be scanned.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | `$CI_COMMIT_SHA`                         |

## Development

### Build image

```sh
docker build -t container-scanning .
```

### Run locally

See the [usage](#how-to-use) section.

### Tests

To run the tests:

1. Unit tests
    ```sh
    go test ./...
    ```
1. Integration tests
    ```
    CLAIR_VULNERABILITIES_DB_URL=`docker-machine ip default` ./test.sh
    ```

## How to update the upstream Scanner

1. Check for the latest versions of `clair` and `klar` at https://github.com/coreos/clair/tags and https://github.com/optiopay/klar/tags
1. Compare with the values of `CLAIR_REPO_TAG` and `KLAR_EXECUTABLE_VERSION` in the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile#L19-23)
1. If an update is available, create a branch and bump the version in [CHANGELOG.md](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/CHANGELOG.md#L3)
1. Edit the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile) and change the following environment variable default values:
      1. [CLAIR_REPO_TAG](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile#L20)
      1. [KLAR_EXECUTABLE_VERSION](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile#L23)
      1. [KLAR_EXECUTABLE_SHA](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile#L26)
1. Create a merge request which will automatically build and tag a new analyzer image using the following form: `registry.gitlab.com/gitlab-org/security-products/analyzers/klar/tmp:af864bd61230d3d694eb01d6205b268b4ad63ac0` where the tag is the `SHA` of the most recent commit
1. Create a new branch in the [container-scanning test project](https://gitlab.com/gitlab-org/security-products/tests/container-scanning) and do the following:
      1. Modify the [container_scanning section of .gitlab-ci.yml](https://gitlab.com/gitlab-org/security-products/tests/container-scanning/blob/d5ec8d5f0ce827fd6a0b0017e657a1302b278e89/.gitlab-ci.yml#L21-27) to reference the new analyzer image:
          ```
          container_scanning:
            allow_failure: false
            # the following is the only line you should need to add here
            image: registry.gitlab.com/gitlab-org/security-products/analyzers/klar/tmp:af864bd61230d3d694eb01d6205b268b4ad63ac0
            variables:
              GIT_STRATEGY: fetch
              CLAIR_DB_IMAGE_TAG: "2019-09-04"
            artifacts:
              paths: [gl-container-scanning-report.json]
          ```
      1. Trigger the pipeline for the above branch in the `container-scanning test project` and make sure it passes
1. Merge the request created in step `5.` and follow the [release process](#versioning-and-release-process) to publish this update.

## Versioning and release process

With the initial release of the Klar analyzer in `12.3`, the associated vendored template was using a `12-3-stable` docker image tag. This [has been removed](https://gitlab.com/gitlab-org/gitlab/merge_requests/18343) and starting with GitLab `12.4`, it follows the usual release process as any other analyzer and doesn't need to be released as an `x-y-stable` docker image tag.

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see
the [LICENSE](LICENSE) file.
