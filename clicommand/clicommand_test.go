package clicommand_test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/clicommand"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/testutils"
)

func TestAnalyzeAndConvert(t *testing.T) {
	var cases = []struct {
		Name, PathToWhitelistFile, PathToExpectedFile string
	}{
		{"when all vulnerabilities have been whitelisted",
			"clair-everything-whitelisted.yml", "common-report-empty.json"},
		{"when all but a single vulnerability have been whitelisted",
			"clair-whitelist.yml", "common-report-with-one-unapproved-cve.json"},
		{"when no whitelist exists", "", "common-report-with-all-unapproved-cves.json"},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			os.Setenv(environment.EnvVarDockerImage, testutils.DefaultDockerImage)
			defer func() { testutils.UnsetEnvVars(t) }()

			tempDir := testutils.TempDir()
			defer testutils.CleanUp()

			flagSet := testutils.TestFlagSet(tempDir)
			if tc.PathToWhitelistFile != "" {
				flagSet.String("whitelist", testutils.FixturesPath(tc.PathToWhitelistFile), "")
			}

			c := cli.NewContext(nil, flagSet, nil)

			err := clicommand.AnalyzeAndConvert(c)
			if err != nil {
				t.Errorf("Expected no err: %s", err)
			}

			pathToActualFile := filepath.Join(tempDir, command.ArtifactNameContainerScanning)
			pathToExpectedFile := testutils.FixturesPath(tc.PathToExpectedFile)

			success, actualContent, expectedContent, err := testutils.CompareJSONFiles(
				pathToActualFile, pathToExpectedFile)

			if err != nil {
				t.Errorf("Error encountered while comparing JSON with file: %s", err)
			}

			if !success {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", expectedContent, actualContent)
			}
		})
	}

	t.Run("when the docker image is not provided", func(t *testing.T) {
		os.Unsetenv(environment.EnvVarDockerImage)
		flagSet := testutils.TestFlagSet("")
		c := cli.NewContext(nil, flagSet, nil)

		err := clicommand.AnalyzeAndConvert(c)

		expectedErrStr := "a Docker image value must be provided"

		if err == nil {
			t.Fatalf("Expected error string '%s', received no err", expectedErrStr)
		}

		if err.Error() != expectedErrStr {
			t.Errorf("Expected error string '%s', received '%s'", expectedErrStr, err.Error())
		}
	})
}
