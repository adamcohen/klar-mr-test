package testutils

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"github.com/pkg/errors"
)

const (
	// DefaultDockerImage is used in various tests
	DefaultDockerImage = "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e"
)

var tempDir string

// TempDir creates a temporary directory to use for testing
func TempDir() string {
	dir, err := ioutil.TempDir("", "container-scanning-temp")
	if err != nil {
		panic("Expected `TempDir` to return successfully")
	}

	tempDir = dir

	return dir
}

// CleanUp removes temporary files created during testing
func CleanUp() {
	os.RemoveAll(tempDir)
}

// FixturesPath is the directory where the test fixtures are located
func FixturesPath(pathToFile string) string {
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "..")

	return filepath.Join(dir, "testdata", pathToFile)
}

// KlarBinaryPath returns the path to the mock klar binary
func KlarBinaryPath() string {
	return filepath.Join(FixturesPath("mocks/klarmock.sh"))
}

// TestFlagSet returns the default FlagSet used for testing purposes
func TestFlagSet(artifactDir string) *flag.FlagSet {
	set := flag.NewFlagSet("test", 0)
	set.String("klar-binary-path", KlarBinaryPath(), "")
	set.String("artifact-dir", artifactDir, "")

	return set
}

// MatchJSON compares two JSON strings, regardless of order
// TODO: find out if we can use the Gomega package which provides MatchJSON(json interface{})
// See https://onsi.github.io/gomega/
func MatchJSON(s1, s2 string) (bool, error) {
	var o1 interface{}
	var o2 interface{}

	var err error
	err = json.Unmarshal([]byte(s1), &o1)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 1 :: %s", err.Error())
	}
	err = json.Unmarshal([]byte(s2), &o2)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 2 :: %s", err.Error())
	}

	return reflect.DeepEqual(o1, o2), nil
}

// CompareJSONFiles compares the JSON contents of pathToActualFile against pathToExpectedFile,
// ignoring order
func CompareJSONFiles(pathToActualFile, pathToExpectedFile string) (bool, string, string, error) {
	actualFileContents, err := readFile(pathToActualFile)
	if err != nil {
		return false, "", "", err
	}

	expectedFileContents, err := readFile(pathToExpectedFile)
	if err != nil {
		return false, "", "", err
	}

	match, err := MatchJSON(string(actualFileContents), string(expectedFileContents))
	if err != nil {
		return false, "", "", errors.Wrap(err, "Error encountered while comparing JSON")
	}

	return match, actualFileContents, expectedFileContents, nil
}

// UnsetEnvVars unsets environment variables that may be set during testing
func UnsetEnvVars(t *testing.T) {
	for _, envVar := range []string{"DOCKER_IMAGE", "CI_APPLICATION_REPOSITORY", "KLAR_TRACE",
		"CI_APPLICATION_TAG", "CI_COMMIT_REF_SLUG", "CI_REGISTRY_IMAGE", "CI_COMMIT_SHA"} {
		err := os.Unsetenv(envVar)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}
	}
}

func readFile(pathToFile string) (string, error) {
	if _, err := os.Stat(pathToFile); os.IsNotExist(err) {
		return "", errors.Wrap(err,
			fmt.Sprintf("Expected file with path '%s' to exist", pathToFile))
	}

	contents, err := ioutil.ReadFile(pathToFile)
	if err != nil {
		return "", errors.Wrap(err,
			fmt.Sprintf("Error encountered while attempting to read file with path '%s'", pathToFile))
	}

	return string(contents), nil
}
