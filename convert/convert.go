package convert

import (
	"fmt"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	// ScannerID identifies the analyzer that generated the report
	ScannerID = "klar"
	// ScannerName identifies the analyzer that generated the report
	ScannerName = "klar"
)

// Convert transforms a list of vulnerabilities from klar (https://github.com/optiopay/klar)
// format to the Security Products Common Format without applying any whitelisting
func Convert(klarJSONReport io.Reader, dockerImageName string) (*issue.Report, error) {
	var scanner = issue.Scanner{
		ID:   ScannerID,
		Name: ScannerName,
	}

	klarVulnerabilities, err := parseKlarVulnerabilities(klarJSONReport)
	if err != nil {
		return nil, err
	}

	issues := []issue.Issue{}
	for _, v := range klarVulnerabilities {
		r := result{v, dockerImageName}

		// convert the KlarVulnerability into a Security Products Common Format issue
		issues = append(issues, issue.Issue{
			Category:    issue.CategoryContainerScanning,
			Scanner:     scanner,
			Message:     r.message(),
			Description: r.issueDescription(),
			Solution:    r.solution(),
			Severity:    r.level(),
			Links:       issue.NewLinks(r.Link),
			Location:    r.location(),
			CompareKey:  r.compareKey(),
			Identifiers: r.identifiers(),
			Confidence:  issue.ConfidenceLevelUnknown,
		})
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

type result struct {
	klarVulnerability
	dockerImageName string
}

func (r result) compareKey() string {
	return strings.Join([]string{r.NamespaceName, r.FeatureName, r.Name}, ":")
}

func (r result) location() issue.Location {
	return issue.Location{
		OperatingSystem: r.NamespaceName,
		Image:           r.dockerImageName,
		Dependency: issue.Dependency{
			Package: issue.Package{r.FeatureName},
			Version: r.FeatureVersion,
		},
	}
}

func (r result) message() string {
	if r.Name != "" && r.FeatureName != "" {
		return fmt.Sprintf("%s in %s", r.Name, r.FeatureName)
	}

	return r.Name
}

func (r result) issueDescription() string {
	if r.Description != "" {
		return r.Description
	}

	if r.FeatureName != "" && r.FeatureVersion != "" {
		return fmt.Sprintf("%s:%s is affected by %s", r.FeatureName, r.FeatureVersion, r.Name)
	}

	if r.FeatureName != "" {
		return fmt.Sprintf("%s is affected by %s", r.FeatureName, r.Name)
	}

	if r.NamespaceName != "" {
		return fmt.Sprintf("%s is affected by %s", r.NamespaceName, r.Name)
	}

	return ""
}

func (r result) solution() string {
	if r.FixedBy != "" && r.FeatureName != "" && r.FeatureVersion != "" {
		return fmt.Sprintf("Upgrade %s from %s to %s", r.FeatureName, r.FeatureVersion, r.FixedBy)
	}

	if r.FixedBy != "" && r.FeatureName != "" {
		return fmt.Sprintf("Upgrade %s to %s", r.FeatureName, r.FixedBy)
	}

	if r.FixedBy != "" {
		return fmt.Sprintf("Upgrade %s", r.FixedBy)
	}

	return ""
}

// level returns the normalized severity.
func (r result) level() issue.SeverityLevel {
	switch r.Severity {
	case "Critical", "Defcon1":
		return issue.SeverityLevelCritical
	case "High":
		return issue.SeverityLevelHigh
	case "Medium":
		return issue.SeverityLevelMedium
	case "Negligible", "Low":
		return issue.SeverityLevelLow
	}

	return issue.SeverityLevelUnknown
}

// identifiers returns the normalized identifiers of the vulnerability.
func (r result) identifiers() []issue.Identifier {
	if identifier, ok := issue.ParseIdentifierID(r.Name); ok {
		return []issue.Identifier{identifier}
	}

	parts := strings.SplitN(r.Name, "-", 2)

	// ParseIdentifierID doesn't handle Red Hat Security Advisory (RHSA) or Oracle Linux
	// (ELSA) vulnerabilities, so I had to add the following to properly capture the
	// identifier.
	// TODO: move this logic for handling RHSA/ELSA to ParseIdentifierID
	// see https://gitlab.com/gitlab-org/gitlab/issues/36441
	switch strings.ToUpper(parts[0]) {
	case "RHSA":
		return rhsaIdentifier(r.Name)
	case "ELSA":
		return elsaIdentifier(r.Name)
	}

	log.Warnf("Unknown vulnerability ID '%s', whitelist support disabled for vulnerability.  Please report this by submitting an issue to GitLab by visiting https://gitlab.com/gitlab-org/gitlab/issues/new?issue.", r.Name)
	return []issue.Identifier{}
}

// rhsaIdentifier returns a structured Identifier for a given RHSA-ID
// Given ID must follow this format: RHSA-YYYY:NNNN
func rhsaIdentifier(ID string) []issue.Identifier {
	var IdentifierTypeRHSA issue.IdentifierType = "rhsa"

	return []issue.Identifier{
		issue.Identifier{
			Type:  IdentifierTypeRHSA,
			Name:  ID,
			Value: ID,
			URL:   fmt.Sprintf("https://access.redhat.com/errata/%s", ID),
		},
	}
}

// elsaIdentifier returns a structured Identifier for a given ELSA-ID
func elsaIdentifier(ID string) []issue.Identifier {
	var IdentifierTypeELSA issue.IdentifierType = "elsa"

	return []issue.Identifier{
		issue.Identifier{
			Type:  IdentifierTypeELSA,
			Name:  ID,
			Value: ID,
			URL:   fmt.Sprintf("https://linux.oracle.com/errata/%s.html", ID),
		},
	}
}
