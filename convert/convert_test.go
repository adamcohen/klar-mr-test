package convert_test

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/testutils"
)

func TestConvert(t *testing.T) {
	var scanner = issue.Scanner{
		ID:   convert.ScannerID,
		Name: convert.ScannerName,
	}

	t.Run("Convert", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "High": [
		      {
		        "Name": "CVE-2019-9513",
		        "NamespaceName": "debian:10",
		        "Description": "Some HTTP/2 implementations are vulnerable to resource loops.",
		        "Link": "https://security-tracker.debian.org/tracker/CVE-2019-9513",
		        "Severity": "High",
		        "Metadata": {
		          "NVD": {
		            "CVSSv2": {
		              "Score": 7.8,
		              "Vectors": "AV:N/AC:L/Au:N/C:N/I:N"
		            }
		          }
		        },
		        "FixedBy": "1.36.0-2+deb10u1",
		        "FeatureName": "nghttp2",
		        "FeatureVersion": "1.36.0-2"
		    }],
		    "Defcon1": [
		      {
		        "Name": "RHSA-2015:0092",
		        "NamespaceName": "centos:6",
		        "Description": "A heap-based buffer overflow was found in glibc.",
		        "Link": "https://access.redhat.com/errata/RHSA-2015:0092",
		        "Severity": "Defcon1",
		        "FeatureName": "glibc-common",
		        "FeatureVersion": "2.12-1.149.el6"
		      }],
		    "Medium": [
		      {
		        "Name": "ELSA-2019-2471",
		        "NamespaceName": "oracle:6",
		        "Description": "[1.0.1e-58.0.1] - Oracle bug 28730228",
		        "Link": "http://linux.oracle.com/errata/ELSA-2019-2471.html",
		        "Severity": "Medium",
		        "FixedBy": "0:1.0.1e-58.0.1.el6_10",
		        "FeatureName": "openssl",
		        "FeatureVersion": "1.0.1e-57.0.6.el6"
		      }]
		  }
		}`

		r := strings.NewReader(in)
		want := &issue.Report{
			Version: issue.CurrentVersion(),
			Vulnerabilities: []issue.Issue{
				{
					Category:    "container_scanning",
					Message:     "ELSA-2019-2471 in openssl",
					Description: "[1.0.1e-58.0.1] - Oracle bug 28730228",
					CompareKey:  "oracle:6:openssl:ELSA-2019-2471",
					Severity:    issue.SeverityLevelMedium,
					Confidence:  issue.ConfidenceLevelUnknown,
					Solution:    "Upgrade openssl from 1.0.1e-57.0.6.el6 to 0:1.0.1e-58.0.1.el6_10",
					Scanner:     scanner,
					Location: issue.Location{
						Dependency: issue.Dependency{
							Package: issue.Package{Name: "openssl"},
							Version: "1.0.1e-57.0.6.el6",
						},
						OperatingSystem: "oracle:6",
						Image:           testutils.DefaultDockerImage,
					},

					Identifiers: []issue.Identifier{
						{
							Type:  "elsa",
							Name:  "ELSA-2019-2471",
							Value: "ELSA-2019-2471",
							URL:   "https://linux.oracle.com/errata/ELSA-2019-2471.html",
						},
					},
					Links: []issue.Link{
						{Name: "", URL: "http://linux.oracle.com/errata/ELSA-2019-2471.html"},
					},
				},
				{
					Category:    "container_scanning",
					Message:     "CVE-2019-9513 in nghttp2",
					Description: "Some HTTP/2 implementations are vulnerable to resource loops.",
					CompareKey:  "debian:10:nghttp2:CVE-2019-9513",
					Severity:    issue.SeverityLevelHigh,
					Confidence:  issue.ConfidenceLevelUnknown,
					Solution:    "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
					Scanner:     scanner,
					Location: issue.Location{
						Dependency: issue.Dependency{
							Package: issue.Package{Name: "nghttp2"},
							Version: "1.36.0-2",
						},
						OperatingSystem: "debian:10",
						Image:           testutils.DefaultDockerImage,
					},

					Identifiers: []issue.Identifier{
						{
							Type:  "cve",
							Name:  "CVE-2019-9513",
							Value: "CVE-2019-9513",
							URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-9513",
						},
					},
					Links: []issue.Link{
						{Name: "", URL: "https://security-tracker.debian.org/tracker/CVE-2019-9513"},
					},
				},
				{
					Category:    "container_scanning",
					Message:     "RHSA-2015:0092 in glibc-common",
					Description: "A heap-based buffer overflow was found in glibc.",
					CompareKey:  "centos:6:glibc-common:RHSA-2015:0092",
					Severity:    issue.SeverityLevelCritical,
					Confidence:  issue.ConfidenceLevelUnknown,
					Solution:    "",
					Scanner:     scanner,
					Location: issue.Location{
						Dependency: issue.Dependency{
							Package: issue.Package{Name: "glibc-common"},
							Version: "2.12-1.149.el6",
						},
						OperatingSystem: "centos:6",
						Image:           testutils.DefaultDockerImage,
					},

					Identifiers: []issue.Identifier{
						{
							Type:  "rhsa",
							Name:  "RHSA-2015:0092",
							Value: "RHSA-2015:0092",
							URL:   "https://access.redhat.com/errata/RHSA-2015:0092",
						},
					},
					Links: []issue.Link{
						{Name: "", URL: "https://access.redhat.com/errata/RHSA-2015:0092"},
					},
				},
			},
			Remediations:    []issue.Remediation{},
			DependencyFiles: []issue.DependencyFile{},
		}

		got, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("Convert - when FixedBy, FeatureName and FeatureVersion are available", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "Defcon1": [
		      {
		        "Name": "RHSA-2015:0092",
		        "Severity": "Defcon1",
		        "FeatureName": "glibc-common",
		        "FeatureVersion": "2.12-1.149.el6",
            "FixedBy": "0:2.12-1.149.el6_6.5"
		      }
        ]
		  }
		}`

		r := strings.NewReader(in)
		convertedReport, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := "Upgrade glibc-common from 2.12-1.149.el6 to 0:2.12-1.149.el6_6.5"
		got := convertedReport.Vulnerabilities[0].Solution

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("Convert - when only FixedBy and FeatureName are available", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "Defcon1": [
		      {
		        "Name": "RHSA-2015:0092",
		        "Severity": "Defcon1",
		        "FeatureName": "glibc-common",
            "FixedBy": "0:2.12-1.149.el6_6.5"
		      }
        ]
		  }
		}`

		r := strings.NewReader(in)
		convertedReport, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := "Upgrade glibc-common to 0:2.12-1.149.el6_6.5"
		got := convertedReport.Vulnerabilities[0].Solution

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("Convert - when only FixedBy is available", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "Defcon1": [
		      {
		        "Name": "RHSA-2015:0092",
		        "Severity": "Defcon1",
            "FixedBy": "0:2.12-1.149.el6_6.5"
		      }
        ]
		  }
		}`

		r := strings.NewReader(in)
		convertedReport, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := "Upgrade 0:2.12-1.149.el6_6.5"
		got := convertedReport.Vulnerabilities[0].Solution

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("Convert - when none of the expected fields are available for a solution", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "Defcon1": [
		      {
		        "Name": "RHSA-2015:0092",
		        "Severity": "Defcon1"
		      }
        ]
		  }
		}`

		r := strings.NewReader(in)
		convertedReport, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := ""
		got := convertedReport.Vulnerabilities[0].Solution

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("Convert - testing unknown identifier", func(t *testing.T) {
		in := `{
		  "LayerCount": 11,
		  "Vulnerabilities": {
		    "Defcon1": [
		      {
		        "Name": "UNKNOWN-2015:0092",
		        "Severity": "Defcon1"
		      }
        ]
		  }
		}`

		r := strings.NewReader(in)
		convertedReport, err := convert.Convert(r, testutils.DefaultDockerImage)

		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := []issue.Identifier{}
		got := convertedReport.Vulnerabilities[0].Identifiers

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
