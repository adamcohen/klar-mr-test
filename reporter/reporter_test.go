package reporter_test

import (
	"path/filepath"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/reporter"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/testutils"
)

func TestReportToFile(t *testing.T) {
	var cases = []struct {
		Name, PathToExpectedFile string
		Report                   *issue.Report
		UnapprovedCVEs           map[string]bool
	}{
		{"when there are only two unapproved CVEs", "common-report-with-two-unapproved-cves.json",
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2012-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2012-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2018-9510",
						Identifiers: []issue.Identifier{{Value: "CVE-2018-9510"}},
						Severity:    issue.SeverityLevelUnknown,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2016-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2016-9513"}},
						Severity:    issue.SeverityLevelMedium,
					},
				},
			},
			map[string]bool{"CVE-2018-9510": true, "CVE-2012-9513": true}},
		{
			"when one of the CVEs cannot be resolved", "common-report-with-unknown-cve.json",
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
						Severity:    issue.SeverityLevelLow,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2012-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2012-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "unknown-os:10:some-package:UNKNOWN-2020-1234",
						Identifiers: []issue.Identifier{},
						Severity:    issue.SeverityLevelHigh,
					},
				},
			},
			map[string]bool{"CVE-2014-9513": true}},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			tempDir := testutils.TempDir()
			defer testutils.CleanUp()

			err := reporter.ReportToFile(tc.Report, tc.UnapprovedCVEs, tempDir)
			if err != nil {
				t.Errorf("Expected no err: %s", err)
			}

			pathToActualFile := filepath.Join(tempDir, command.ArtifactNameContainerScanning)
			pathToExpectedFile := testutils.FixturesPath(tc.PathToExpectedFile)

			success, actualContent, expectedContent, err := testutils.CompareJSONFiles(
				pathToActualFile, pathToExpectedFile)
			if err != nil {
				t.Errorf("Error encountered while comparing JSON with file: %s", err)
			}

			if !success {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", expectedContent, actualContent)
			}
		})
	}
}

func TestExtIDForVulnerability(t *testing.T) {
	t.Run("when an identifier exists", func(t *testing.T) {
		vulnerability := issue.Issue{
			CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
			Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
			Severity:    issue.SeverityLevelHigh,
		}

		want := "CVE-2014-9513"
		got, ok := reporter.ExtIDForVulnerability(vulnerability)
		if !ok {
			t.Errorf("Expected ok to be true, but was false")
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when an identifier does not exist", func(t *testing.T) {
		vulnerability := issue.Issue{
			CompareKey:  "oracle:5:glibc-common:UNKNOWN-2017-1479",
			Identifiers: []issue.Identifier{},
			Severity:    issue.SeverityLevelHigh,
		}

		want := ""
		got, ok := reporter.ExtIDForVulnerability(vulnerability)
		if ok {
			t.Errorf("Expected ok to be false, but was true")
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
