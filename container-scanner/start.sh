#!/usr/bin/env sh
set -e

function infoMsg() {
  echo ""
  echo -e "\033[1;34m[INFO] ▶ $1\033[0m"
}

function noticeMsg() {
  echo ""
  echo -e "\033[1;36m[NOTICE] ▶ $1\033[0m"
}

function warningMsg() {
  echo ""
  echo -e "\033[1;33m[WARNING] ▶ $1\033[0m"
}

function errorMsg() {
  echo ""
  echo -e "\033[1;31m[ERROR] ▶ $1\033[0m"
}

function debugMsg() {
  echo ""
  echo -e "\033[0;36m[DEBUG] ▶ $1\033[0m"
}

# workaround for this bug https://gitlab.com/gitlab-org/gitlab-runner/issues/1380
[ -f /ran.txt ] && exit 0 || >/ran.txt

# Defining two new variables based on GitLab's CI/CD predefined variables
# https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}

# Prior to this, you need to have the Container Registry running for your project and set up a build job
# with at least the following steps:
#
# docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA .
# docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA

# this is necessary to enable the yarn script to output colour
export FORCE_COLOR=1

# if the KUBERNETES_PORT environment variable is set, then we assume this is being run within the context
# of a kubernetes cluster, therefore we can't rely on the `clair-vulnerabilities-db` service alias url
# and must set this explicitly to the localhost address
if [[ -n "$KUBERNETES_PORT" ]]
then
    warningMsg "Detected KUBERNETES_PORT environment variable, using 127.0.0.1 for CLAIR_VULNERABILITIES_DB_URL"
    export CLAIR_VULNERABILITIES_DB_URL="127.0.0.1" ;
fi

# by default, use the `clair-vulnerabilities-db` service alias url
# (https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#available-settings-for-services)
# defined in the job template of Container Scanning (Container-Scanning.gitlab-ci.yml), however,
# it can be useful to override this value against your local postgres instance when testing locally
export CLAIR_VULNERABILITIES_DB_URL=${CLAIR_VULNERABILITIES_DB_URL:-clair-vulnerabilities-db}

mkdir -p /var/log/supervisor

# allow overriding the postgres vulnerabilities database url used by clair
sed -i s/POSTGRES-VULNERABILITIES-DB-URL/${CLAIR_VULNERABILITIES_DB_URL}/g /container-scanner/clair/config.yaml

/usr/bin/supervisord -c /container-scanner/supervisord/supervisord.conf

retries=0

noticeMsg "Waiting for Clair daemon to start"

while( ! wget -T 10 -q -O /dev/null http://localhost:6060/v1/namespaces ) do
  warningMsg "Clair daemon not ready, waiting 1 second before retrying. Clair log contents:"
  supervisorctl tail clair
  sleep 1
  if [ $retries -eq 10 ]
  then
    errorMsg " Timeout, aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

noticeMsg "Clair daemon started successfully."

# scan the container and output the report
/analyzer r ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
