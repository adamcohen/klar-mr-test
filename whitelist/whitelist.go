package whitelist

import (
	"os"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

// Whitelist format is specified here:
// https://github.com/arminc/clair-scanner/blob/master/example-whitelist.yaml
type Whitelist struct {
	GeneralWhitelist map[string]string            //[key: CVE and value: CVE description]
	Images           map[string]map[string]string // image name with [key: CVE and value: CVE description]
}

// Parse reads the whitelist file and parses it
func Parse(whitelistFile string) (Whitelist, error) {
	whitelistTmp := Whitelist{}

	if _, err := os.Stat(whitelistFile); os.IsNotExist(err) {
		log.Warnf("Whitelist file with path '%s' does not exist, skipping", whitelistFile)
		return whitelistTmp, nil
	}

	f, err := os.Open(whitelistFile)
	if err != nil {
		return whitelistTmp, errors.Wrapf(err, "unable to open whitelist file with path '%s'", whitelistFile)
	}
	defer f.Close()

	if err = yaml.NewDecoder(f).Decode(&whitelistTmp); err != nil {
		return whitelistTmp, errors.Wrapf(err, "Unable to parse whitelist file with path '%s', could not decode.",
			whitelistFile)
	}

	log.Infof("Found whitelist file with path '%s'", whitelistFile)

	return whitelistTmp, nil
}
