package whitelist_test

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/testutils"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/whitelist"
)

func TestParseWhitelistFile(t *testing.T) {
	t.Run("when there are only two unapproved CVEs", func(t *testing.T) {
		got, err := whitelist.Parse(testutils.FixturesPath("clair-whitelist.yml"))
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := whitelist.Whitelist{
			GeneralWhitelist: map[string]string{
				"CVE-2019-8696":  "cups",
				"CVE-2014-8166":  "cups",
				"CVE-2017-18248": "cups",
			},
			Images: map[string]map[string]string{
				"registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256": {
					"CVE-2018-4180": "cups",
				},
				"your.private.registry:5000/centos": {
					"RHSA-2015:1419": "libxml2",
					"RHSA-2015:1447": "grep",
				},
			},
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the path to the whitelist doesn't exist", func(t *testing.T) {
		// empty whitelist should be returned
		want := whitelist.Whitelist{}
		got, err := whitelist.Parse(testutils.FixturesPath("non-existent-whitelist.yml"))
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the whitelist doesn't have the correct format", func(t *testing.T) {
		// an error should be returned
		_, err := whitelist.Parse(testutils.FixturesPath("invalid-whitelist.yml"))
		if err == nil {
			t.Error("Expected err, got nil")
		}

		want := "Unable to parse whitelist file with path"
		got := err.Error()

		if !strings.Contains(got, want) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
