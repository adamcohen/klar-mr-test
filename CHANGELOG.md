# klar analyzer changelog

## v2.0.0

- Rewrite converter code from `Node.js` to `Go`
- Output container scanning report using the [Security Products Common Format](https://gitlab.com/gitlab-org/security-products/analyzers/common)

## v1.0.3

- Add default values for DOCKER_USER/DOCKER_PASSWORD to allow using private GitLab Container Registry without additional configuration

## v1.0.2

- Fix whitelist handling by using the `clair-whitelist.yml` file instead of `clair-whitelist.yaml`
- Don't use the image version when matching against image names in the whitelist file

## v1.0.1

- Fix Kubernetes support by setting Clair DB URL to `127.0.01` when running in the context of a kubernetes executor (!6)

## v1.0.0

- Initial release
