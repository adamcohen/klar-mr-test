package scanner_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/whitelist"
)

func TestRemoveDockerImageTag(t *testing.T) {
	t.Run("when the registry is private", func(t *testing.T) {
		want := "some.private.registry:5000/centos"
		got := scanner.RemoveDockerImageTag("some.private.registry:5000/centos:6.6")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the registry is public", func(t *testing.T) {
		want := "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256"
		got := scanner.RemoveDockerImageTag("registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when the registry does not have a version tag", func(t *testing.T) {
		want := "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256"
		got := scanner.RemoveDockerImageTag("registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256")

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestGetImageVulnerabilities(t *testing.T) {
	whitelist := map[string]map[string]string{
		"some.private.registry:5000/centos": map[string]string{
			"RHSA-2017:2832": "nss-tools",
			"RHSA-2015:1419": "libxml2",
		},
		"non-matching-image-name": map[string]string{"RHSA-2018:2180": ""},
	}

	t.Run("when a matching image is found", func(t *testing.T) {
		want := map[string]string{
			"RHSA-2017:2832": "nss-tools",
			"RHSA-2015:1419": "libxml2",
		}

		got := scanner.GetImageVulnerabilities("some.private.registry:5000/centos:6.6", whitelist)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when a matching image is not found", func(t *testing.T) {
		var want map[string]string

		got := scanner.GetImageVulnerabilities("another.private.registry:5000/alpine:latest", whitelist)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}

func TestCheckForUnapprovedVulnerabilities(t *testing.T) {
	whitelist := whitelist.Whitelist{
		GeneralWhitelist: map[string]string{
			"RHSA-2015:1634": "sqlite",
			"RHSA-2016:1626": "python",
			"RHSA-2019:2471": "openssl",
		},
		Images: map[string]map[string]string{
			"some.private.registry:5000/centos": map[string]string{
				"RHSA-2015:1419": "libxml2",
			},
			"non-matching-image-name": map[string]string{"RHSA-2018:2180": ""},
		},
	}

	report := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				CompareKey:  "centos:6:sqlite:RHSA-2015:1634",
				Identifiers: []issue.Identifier{{Value: "RHSA-2015:1634"}},
				Severity:    issue.SeverityLevelMedium,
			},
			{
				CompareKey:  "centos:6:nss:RHSA-2017:2832",
				Identifiers: []issue.Identifier{{Value: "RHSA-2017:2832"}},
				Severity:    issue.SeverityLevelHigh,
			},
			{
				CompareKey:  "centos:6:libxml2:RHSA-2015:1419",
				Identifiers: []issue.Identifier{{Value: "RHSA-2015:1419"}},
				Severity:    issue.SeverityLevelHigh,
			},
		},
	}

	t.Run("when a vulnerability matches only the general whitelist", func(t *testing.T) {
		// it should strip out only a single approved vulnerability
		want := map[string]bool{
			"RHSA-2017:2832": true,
			"RHSA-2015:1419": true,
		}

		got := scanner.CheckForUnapprovedVulnerabilities("registry.gitlab.com/centos:6.6", report, whitelist)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when vulnerability matches are found in the general and image whitelist", func(t *testing.T) {
		// it should strip out both approved vulnerabilities
		want := map[string]bool{
			"RHSA-2017:2832": true,
		}

		got := scanner.CheckForUnapprovedVulnerabilities("some.private.registry:5000/centos:6.6", report, whitelist)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
