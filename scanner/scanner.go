package scanner

import (
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/reporter"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/whitelist"
)

// CheckForUnapprovedVulnerabilities checks if the found vulnerabilities are approved or not in the whitelist
func CheckForUnapprovedVulnerabilities(dockerImageName string, report *issue.Report,
	whitelist whitelist.Whitelist) map[string]bool {

	unapproved := map[string]bool{}
	imageVulnerabilities := GetImageVulnerabilities(dockerImageName, whitelist.Images)

	for _, vulnerability := range report.Vulnerabilities {
		vulnerable := true
		cve, ok := reporter.ExtIDForVulnerability(vulnerability)

		if !ok {
			continue
		}

		//Check if the vulnerability exists in the GeneralWhitelist
		if vulnerable {
			if _, exists := whitelist.GeneralWhitelist[cve]; exists {
				vulnerable = false
			}
		}

		//If not in GeneralWhitelist check if the vulnerability exists in the imageVulnerabilities
		if vulnerable && len(imageVulnerabilities) > 0 {
			if _, exists := imageVulnerabilities[cve]; exists {
				vulnerable = false
			}
		}
		if vulnerable {
			unapproved[cve] = true
		}
	}
	return unapproved
}

// RemoveDockerImageTag takes as input a docker image path with a tag and returns only the image portion
// without including the tag details
//
// Examples:
//
// given `some.private.registry:5000/gl-centos:6.6`
// returns `some.private.registry:5000/gl-centos`
//
// given `centos:6.6`
// returns `centos`
func RemoveDockerImageTag(dockerImageName string) string {
	// if the dockerImageName doesn't contain an image tag version, then just return
	// the dockerImageName
	if !strings.Contains(dockerImageName, ":") {
		return dockerImageName
	}

	dockerImageNameComponents := strings.Split(dockerImageName, ":")

	// check if the image name has the form some.private.registry:5000/gl-centos:6.6
	if len(dockerImageNameComponents) > 2 {
		return strings.Join(dockerImageNameComponents[0:2], ":")
	}

	return dockerImageNameComponents[0]
}

// GetImageVulnerabilities returns image specific whitelist of vulnerabilities from whitelistImageVulnerabilities
func GetImageVulnerabilities(dockerImageName string,
	whitelistImageVulnerabilities map[string]map[string]string) map[string]string {

	var imageVulnerabilities map[string]string

	if val, exists := whitelistImageVulnerabilities[RemoveDockerImageTag(dockerImageName)]; exists {
		imageVulnerabilities = val
	}
	return imageVulnerabilities
}
