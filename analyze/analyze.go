package analyze

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
)

var (
	unableToPullDockerImageRegexp = regexp.MustCompile(`Can't pull image: Docker Registry responded with unsupported Content-Type: application/json\n`)
	redactHeaderAuthRegex         = regexp.MustCompile(`(?i)(authorization:) .*`)
	redactBodyAuthRegex           = regexp.MustCompile(`(?i)("authorization":)".*?"`)
)

// Analyze executes the klar binary on the given docker image and returns
// the output produced by klar
func Analyze(klarBinaryPath, dockerImageName string) (io.ReadCloser, error) {
	environment.SetEnvironmentVariablesForKlar()

	log.Infof("Scanning container from registry '%s' for vulnerabilities with severity level '%s' or higher with klar '%s' and clair '%s'",
		dockerImageName, environment.SeverityLevelThreshold(),
		environment.KlarExecutableVersion(), environment.ClairRepoTag())

	// configure exec command to run the container scan on the given docker image using klar
	cmd := exec.Command(klarBinaryPath, dockerImageName)

	// set the environment variables for the process
	cmd.Env = os.Environ()

	var stderr bytes.Buffer
	// store stderr output from the klar command in order to intercept the
	// error and provide additional error handling before passing the
	// message to the console
	cmd.Stderr = &stderr

	// initiate the container scan on the given docker image using klar
	output, err := cmd.Output()

	// if KLAR_TRACE has been set, then the debugging details will be output to stderr.
	if environment.KlarTrace() {
		log.Warn("KLAR_TRACE=true detected, printing trace for container scan:")
		// TODO: use cmd.Start() instead of cmd.Output() above, and read contents of StderrPipe,
		// otherwise the klar command will block and no output will be provided if an error occurs
		// and KLAR_TRACE has been enabled
		log.Warn(redactStr(stderr.String()))
	}

	// if no error occurred, either there were no vulnerabilities, or the number of
	// vulnerabilities was less than the CLAIR_THRESHOLD value
	if err == nil {
		return ioutil.NopCloser(bytes.NewReader(output)), nil
	}

	if exiterr, ok := err.(*exec.ExitError); ok {
		// klar returns with exit code 1 when the number of vulnerabilities is greater than
		// the CLAIR_THRESHOLD value, so this means the container scan was successful and
		// vulnerabilities were found, so return as normal
		if exitCode := exiterr.ExitCode(); exitCode == 1 {
			return ioutil.NopCloser(bytes.NewReader(output)), nil
		}
	}

	errMsg := parseError(stderr, err)
	// if we get here, then klar has reported an actual error
	log.Errorf("Error encountered while scanning container '%s': %s", dockerImageName, errMsg)

	// perform an additional check on the error to provide more helpful output
	if strings.Contains(errMsg, "server gave HTTP response to HTTPS client") {
		logMismatchedProtocolError("an insecure (HTTP)", "a secure protocol (HTTPS)")
	} else if unableToPullDockerImageRegexp.MatchString(errMsg) {
		logMismatchedProtocolError("a secure (HTTPS)", "an insecure protocol (HTTP)")
	} else if strings.Contains(errMsg, "Can't pull image: Token request returned 401") {
		logUnauthorizedError()
	}

	return nil, err
}

// redactStr removes the authorization details which klar outputs in stderr
func redactStr(str string) string {
	redactedStr := redactHeaderAuthRegex.ReplaceAllString(str, `$1 [REDACTED]`)
	return redactBodyAuthRegex.ReplaceAllString(redactedStr, `$1"[REDACTED]"`)
}

func logMismatchedProtocolError(actualProto, givenProto string) {
	log.Errorf("It looks like you're attempting to pull an image from %s registry using %s, try setting %s=%v", actualProto, givenProto, environment.EnvVarRegistryInsecure, !environment.RegistryInsecure())
}

func logUnauthorizedError() {
	if environment.DockerCredentialsOverridden() {
		log.Error("Unauthorized response returned from Docker registry. It looks like the DOCKER_USER and DOCKER_PASSWORD environment variables are not valid for this registry.")
	} else {
		log.Error(`Unauthorized response returned from Docker registry. The DOCKER_USER and DOCKER_PASSWORD environment variables have been defaulted to CI_REGISTRY_USER and CI_REGISTRY_PASSWORD respectively, however, perhaps the Docker registry doesn't require authentication, in which case you should configure DOCKER_USER="" and DOCKER_PASSWORD=""`)
	}
}

// stderr contains more descriptive error contents which will only be output when
// running with KLAR_TRACE=true, but we include it when an error occurs to make it
// easier to diagnose the issue.
//
// For example, if we just output the error string, we'll get the following error message:
//
//   Error encountered while scanning container 'oraclelinux:5': exit status 2
//
// If we output stderr here, we'll see the following:
//
//   Error encountered while scanning container 'oraclelinux:5': Invalid options:
//   Clair output level Radical is not supported, only support [Unknown Negligible Low Medium
//   High Critical Defcon1]
//
// parseError will attempt to output the stderr string if it exists, otherwise it will fall back
// to outputting the error string
func parseError(stderr bytes.Buffer, err error) string {
	stderrStr := stderr.String()

	if len(stderrStr) == 0 {
		return err.Error()
	}

	return redactStr(stderrStr)
}
