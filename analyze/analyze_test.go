package analyze_test

import (
	"bytes"
	"os"
	"reflect"
	"testing"

	"github.com/sirupsen/logrus/hooks/test"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/analyze"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/testutils"
)

func TestAnalyze(t *testing.T) {
	t.Run("Analyze", func(t *testing.T) {
		os.Setenv(environment.EnvVarDockerImage, testutils.DefaultDockerImage)
		defer func() { testutils.UnsetEnvVars(t) }()

		got, err := analyze.Analyze(testutils.KlarBinaryPath(), testutils.DefaultDockerImage)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		want := `{"LayerCount":9,"Vulnerabilities":{"High":[{"Description":"Insufficient restriction of IPP filters in CUPS in Google Chrome OS prior to 62.0.3202.74 allowed a remote attacker to execute a command with the same privileges as the cups daemon via a crafted PPD file, aka a printer zeroconfig CRLF issue.","FeatureName":"cups","FeatureVersion":"2.2.1-8+deb9u1","FixedBy":"2.2.1-8+deb9u2","Link":"https://security-tracker.debian.org/tracker/CVE-2017-15400","Metadata":{"NVD":{"CVSSv2":{"Score":9.3,"Vectors":"AV:N/AC:M/Au:N/C:C/I:C"}}},"Name":"CVE-2017-15400","NamespaceName":"debian:9","Severity":"High"}],"Low":[{"Description":"The add_job function in scheduler/ipp.c in CUPS before 2.2.6, when D-Bus support is enabled, can be crashed by remote attackers by sending print jobs with an invalid username, related to a D-Bus notification.","FeatureName":"cups","FeatureVersion":"2.2.1-8+deb9u1","FixedBy":"2.2.1-8+deb9u3","Link":"https://security-tracker.debian.org/tracker/CVE-2017-18248","Metadata":{"NVD":{"CVSSv2":{"Score":3.5,"Vectors":"AV:N/AC:M/Au:S/C:N/I:N"}}},"Name":"CVE-2017-18248","NamespaceName":"debian:9","Severity":"Low"}],"Medium":[{"Description":"In macOS High Sierra before 10.13.5, an issue existed in CUPS. This issue was addressed with improved access restrictions.","FeatureName":"cups","FeatureVersion":"2.2.1-8+deb9u1","FixedBy":"2.2.1-8+deb9u2","Link":"https://security-tracker.debian.org/tracker/CVE-2018-4180","Metadata":{"NVD":{"CVSSv2":{"Score":4.6,"Vectors":"AV:L/AC:L/Au:N/C:P/I:P"}}},"Name":"CVE-2018-4180","NamespaceName":"debian:9","Severity":"Medium"}],"Negligible":[{"Description":"The browsing feature in the server in CUPS does not filter ANSI escape sequences from shared printer names, which might allow remote attackers to execute arbitrary code via a crafted printer name.","FeatureName":"cups","FeatureVersion":"2.2.1-8+deb9u1","Link":"https://security-tracker.debian.org/tracker/CVE-2014-8166","Metadata":{"NVD":{"CVSSv2":{"Score":5.1,"Vectors":"AV:N/AC:H/Au:N/C:P/I:P"}}},"Name":"CVE-2014-8166","NamespaceName":"debian:9","Severity":"Negligible"}],"Unknown":[{"FeatureName":"cups","FeatureVersion":"2.2.1-8+deb9u1","Link":"https://security-tracker.debian.org/tracker/CVE-2019-8696","Name":"CVE-2019-8696","NamespaceName":"debian:9","Severity":"Unknown"}]}}`

		buf := new(bytes.Buffer)
		buf.ReadFrom(got)
		gotStr := buf.String()

		match, err := testutils.MatchJSON(gotStr, want)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		if !match {
			t.Errorf("Wrong result. Expected\n%+v\nbut got\n%+v", want, gotStr)
		}
	})

	t.Run("Analyze - ensure Authorization is redacted when KLAR_TRACE is true", func(t *testing.T) {
		os.Setenv(environment.EnvVarDockerImage, testutils.DefaultDockerImage)
		os.Setenv(environment.EnvVarKlarTrace, "true")
		defer func() { testutils.UnsetEnvVars(t) }()

		hook := test.NewGlobal()
		defer hook.Reset()

		_, err := analyze.Analyze(testutils.KlarBinaryPath(), testutils.DefaultDockerImage)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}

		if hook.LastEntry() == nil {
			t.Errorf("Expected log message to have been output")
		}

		got := hook.LastEntry().Message
		want := `----> HTTP REQUEST:
GET /v2/library/debian/manifests/9.8 HTTP/1.1
Host: registry-1.docker.io
Accept: application/vnd.docker.distribution.manifest.v2+json, application/vnd.docker.distribution.manifest.v1+prettyjws, application/vnd.docker.distribution.manifest.list.v2+json
Authorization: [REDACTED]

<---- HTTP RESPONSE:
HTTP/1.1 401 Unauthorized
AUTHORIZATION: [REDACTED]

----> HTTP REQUEST:
POST /v1/layers HTTP/1.1
Host: localhost:6060
Content-Type: application/json

{"Layer":{"Name":"2d337f","Path":"https://registry-1.docker.io/v2/library/debian/blobs/sha256:e79bb959ec00faf01da52437df4fad4537ec669f60455a38ad583ec2b8f00498","Headers":{"Authorization":"[REDACTED]","Content-Type":"application/json"},"ParentName":"","Format":"Docker","Features":null}}

<---- HTTP RESPONSE:
HTTP/1.1 401 Unauthorized
Content-Length: 188
Content-Type: application/json
Date: Mon, 09 Dec 2019 01:01:37 GMT
Docker-Distribution-Api-Version: registry/2.0
Www-Authenticate: Bearer realm="https://gitlab.com/jwt/auth",service="container_registry",scope="repository:gitlab-org/security-products/dast/webgoat-8.0:pull"
X-Content-Type-Options: nosniff

{"errors":[{"code":"UNAUTHORIZED","message":"authentication required","detail":[{"Type":"repository","Class":"","Name":"gitlab-org/security-products/dast/webgoat-8.0","Action":"pull"}]}]}

----> HTTP REQUEST:
GET /v2/gitlab-org/security-products/dast/webgoat-8.0/manifests/sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e HTTP/1.1
Host: registry.gitlab.com
Accept: application/vnd.docker.distribution.manifest.v2+json, application/vnd.docker.distribution.manifest.v1+prettyjws, application/vnd.docker.distribution.manifest.list.v2+json
Authorization: [REDACTED]
`

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
