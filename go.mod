module gitlab.com/gitlab-org/security-products/analyzers/klar/v2

require (
	github.com/olekukonko/tablewriter v0.0.2
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.5.3
	gopkg.in/yaml.v2 v2.2.5
)

go 1.13
